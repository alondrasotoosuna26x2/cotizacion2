const express = require("express");
const router = express.Router();
const bodyparser = require("body-parser"); 


let datos= [{
    matricula: "2020030316",
    nombre: "Alondra Guadalupe Soto Osuna",
    sexo: 'F', 
    materias: ["Ingles", "Tecnologia 1 ", "base de datos"]
},  {
    matricula:"2020030309",
    nombre:"ACOSTA ORTEGA JESUS HUMBERTO",
    sexo:'M',
    materias:["Ingles", "Tecnologia I", "Base de Datos"]
},
{
    matricula:"2020030309",
    nombre:"ACOSTA VARELA IRVING GUADALUPE",
    sexo:'M',
    materias:["Ingles", "Tecnologia I", "Base de Datos"]
},
{
    matricula:"2020030319",
    nombre:"ALMOGAR VAZQUEZ YARLEN DE JESUS",
    sexo:'F',
    materias:["Ingles", "Tecnologia I", "Base de Datos"]
},
{
    matricula:"2020030323",
    nombre:"HERNANDEZ COLIO MARIANA DE JESUS",
    sexo:'F',
    materias:["Ingles", "Tecnologia I", "Base de Datos"]
},
{
    matricula:"2020030209",
    nombre:"HERNANDEZ IBARGUEN JUSTIN ADILAN",
    sexo:'M',
    materias:["Ingles", "Tecnologia I", "Base de Datos"]
}

]

router.get ('/', (req, res)=>{

    res.render('index.html', {titulo:"Lista de alumnos", listado:datos})



})

router.get("/tablas", (req, res)=>{
    const valores ={
        tabla:req.query.tabla
    }
    res.render('tablas.html', valores); 
})

router.post("/tablas", (req, res)=>{
    const valores ={
        tabla:req.body.tabla
    }
    res.render('tablas.html', valores); 
})

router.get("/cotizacion", (req, res)=>{
    const valores ={
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        totalFin:req.query.totalFin, 
        pagoInicial:req.query.pagoInicial,
        plazos:req.query.plazos, 
        pagoMensual:req.query.pagoMensual
    }
    res.render('cotizacion.html', valores); 
})

router.post("/cotizacion", (req, res)=>{
    const valores ={
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        totalFin:req.body.totalFin, 
        pagoInicial:req.body.pagoInicial,
        plazos:req.body.plazos, 
        pagoMensual:req.body.pagoMensual
    }
    res.render('cotizacion.html', valores); 
})


module.exports=router;